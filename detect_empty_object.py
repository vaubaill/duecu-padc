"""DU-ECU PADC project.

Check and correct images fits image for which the object fits keyword is not set.

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""
import os
import glob
from astropy.io import fits

from duecu_padc_log import log

pthi = './'
ptho = './tmp/'
# list all fits file in pthi
listfits = glob.glob(pthi+'/*.fit*')
listfits.sort()
# loop over files
for fitsi in listfits:
    log.info('Processing image: '+fitsi)
    # make output file name
    fitso = fitsi.replace(pthi,ptho)
    # read fits image
    hdu = fits.open(fitsi)
    hdri = hdu[0].header
    data = hdu[0].data
    hdu.close()  
    # make output header
    hdro = hdri.copy()
    # detect empty object keyword
    if hdri['OBJECT']=='':
        log.warning('Empty object found for image '+fitsi)
        # get object name from file name
        obj = os.path.basename(fitsi).split('.fit')[0].split('-')[0].split('_')[0]
        hdro.set('OBJECT',value=obj,comment='Target Object')
        log.info('Header changed. Object name: '+obj)
        # save output image
        hdu = fits.PrimaryHDU(data,header=hdro)
        hdu.verify('fix')
        #hdulist     = fits.HDUList([hdu])
        hdu.writeto(fitso) #,overwrite=True)
        log.info('Image saved in '+fitso)    
    


