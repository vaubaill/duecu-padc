"""DU-ECU PADC project.

Launch the image checking process before ingestion into VO-Paris, for all images in a given folder.

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""


import os
import argparse
import glob

from duecu_padc_log import log
from check_image import fix_image, verify_image

def launch_fix(pthi,ptho):
    """Launch image and header fix script.

    Parameters
    ----------
    pthi : string
        Input directory name.
    ptho : string
        Output directory name.

    Returns
    -------
    None.

    """
    # creates output directory if needed
    if not os.path.isdir(ptho):
        log.info('Make output directory: '+ptho)
        os.mkdir(ptho)
    # list all fits file in pthi
    listfits = glob.glob(pthi+'/*.fit*')
    # loop over files
    for fitsi in listfits:
        # make output file name
        fitso = fitsi.replace(pthi,ptho)
        # launch fix_image
        try:
            fix_image(fitsi,fitso)
        except:
            msg = '*** Impossible to check and modify image: '+fitsi
            log.error(msg)
            raise IOError(msg)
    log.info('done')
    return

def launch_verify(pthi):
    """Launch image and header verification script.

    Parameters
    ----------
    pthi : string
        Input directory name.

    Returns
    -------
    None.

    """
    # lists all fits files in input path
    filelist = glob.glob(os.path.join(pthi,'*.fit*'))
    # loop over files
    for file in filelist:
        # launch verification
        try:
            verify_image(file)
        except:
            msg = '*** Impossible to verify image: '+file
            log.error(msg)
            raise IOError(msg)
    log.info('done')
    return

if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='launch_check_image arguments.')
    parser.add_argument('-i',default='./reduced/',help='Input directory: this is where all input files are located. Default is: ./reduced/')
    parser.add_argument('-o',default='./padc/',help='Output directory: this is where all output files (to be ingested into VO-PADC) will be saved. Default is: /padc/')
    args = parser.parse_args()
    
    # set configuration file
    pthi = args.i
    ptho = args.o
    
    # fix slash syndrome...
    if not pthi.endswith('/'):
        pthi = pthi + '/'
    if not ptho.endswith('/'):
        ptho = ptho + '/'
    
    # launch fix_image
    launch_fix(pthi,ptho)

    # launch verify_image
    launch_verify(ptho)
    
else:
    log.debug('successfully imported')
