"""DU-ECU PADC project.

Check and correct images to be ingested in VO-Paris database.

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""
import glob
from astropy.io import fits

from duecu_padc_log import log

pthi = './'
ptho = './tmp/'
# list all fits file in pthi
listfits = glob.glob(pthi+'/IC*.fits')
# loop over files
for fitsi in listfits:
    log.info('Processing image: '+fitsi)
    # make output file name
    fitso = fitsi.replace(pthi,ptho)
    # read fits image
    hdu = fits.open(fitsi)
    hdri = hdu[0].header
    data = hdu[0].data
    hdu.close()  
    # make output header
    hdro = hdri.copy()
    # performs the corrections:
    hdro.set('OBJECT',value='IC 5070 Pelican nebula',comment='Target name.')
    # save output image
    hdu = fits.PrimaryHDU(data,header=hdro)
    hdu.verify('fix')
    #hdulist     = fits.HDUList([hdu])
    hdu.writeto(fitso,overwrite=True)
    log.info('Image saved in '+fitso)    
    


