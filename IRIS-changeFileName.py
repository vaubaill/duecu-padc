"""DU-ECU PADC project.

Check and correct images to be ingested in VO-Paris database.

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""
import glob
import shutil

from duecu_padc_log import log

pthi = './'
ptho = './reduced/'
# list all fits file in pthi
listfits = glob.glob(pthi+'/*.fts')
# loop over files
for fitsi in listfits:
    log.info('Processing image: '+fitsi)
    # make output file name
    fitso = fitsi.replace(pthi,ptho).replace('.fts','.fits').replace(' ','')
    # save output image
    shutil.copyfile(fitsi, fitso)
    log.info('Image saved in '+fitso)    
    


