# DU-ECU PADC project

## Image check before ingestion project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024.


## Scope
This project aims to make students observations visible to the rest of the world in general, and other students in particular to promote either joint observations (with other e.g. universities or labs), or educative activities (e.g. measure an asteroid orbit, necessitating data taken over several months or years).

Initially, this project was created for the [DU-ECU (evening astronomy classes)](https://ufe.obspm.fr/formations/diplomes-d-universite/du-ecu-explorer-et-comprendre-l-univers/article/presentation-generale).
However it is open to the rest of the world.

The goal of the DU-ECU PADC project is to make observations available to the rest of the community via the Virtual Observatory.
In practice, data are available at [Paris Observatory VO portal](https://voparis-portal.obspm.fr).

The goal of this python package is to provide a way to check and correct the images before they are pushed to the VO portal database.


# Dependencies

    astropy: www.astropy.org


# Syntax

```  python3 launch_check_image.py -i $inputdir -o $outputdir ```

with:

- inputdir: input directory. This is where all reduced images are located. Default is "reduced".
- outputdir: output directory. This is where new images, all ready to be ingested into the PADC database will be saved. Default is "padc".


# How does it work?

The main script is launch_check_image.py, allowing to perform the image check over many images in a folder.

The core script is check_image.py. It performs image header check and modification if needed. In order not to overwrite original data, output images are saved in a different name. See also launch_check_image.py


# Utilities

The duecu-padc package comes with additional utility scripts.

- list_object.py : list object for each fits file in the current directory. Useful to check is the names are standard before submission to PADC.
Syntax is: ```pyhon3 list_object.py```.

- list_filters.py: list filters of all images in the current directory.
Syntax is: ```pyhon3 list_filters.py```.

- changeObjectName.py: change fits header keyword object. Useful in case an ambiguous names. To be run before the main script.
Output file are written in a different directory. Change the code to fit the files to change.
Syntax is: ```pyhon3 changeObjectName.py```.

- changeHalpha.py: change filter name. Useful for old T120 images, for which the filter name did not correspond to official names. To be run before the main script.
Output file are written in a different directory. Change the code to fit the files to change.
Syntax is: ```pyhon3 changeHalpha.py```.

- bntt.py: used to correct fits header for iamges taken with the old BNTT camera (bnefore 2014). To be run before the main script.
Output file are written in a different directory. Change the code to fit the files to change.
Syntax is: ```pyhon3 bntt.py```.

- detect_empty_object.py: detect empty string in the object fits header card. Useful to later correct the header. See also the list_object.py script.
Syntax is: ```pyhon3 detect_empty_object.py```.

- IRIS-changeTelescopeName.py: useful to change the telescope name header card for images taken with the OHP/IRIS telescope. To be run before the main script.
Output file are written in a different directory. Change the code to fit the files to change.
Syntax is: ```pyhon3 IRIS-changeTelescopeName.py```.

- IRIS-changeFileName.py: useful to change the file names, in particular get rid of spaces, for images taken with the OHP/IRIS telescope. To be run before the main script.
Output file are written in a different directory. Change the code to fit the files to change.
Syntax is: ```pyhon3 IRIS-changeFileName.py```.
Note: the IRIS-changeTelescopeName.py also changes the file name, so this script may be forgotten.

- IRIS-changeObjectName.py: change fits header keyword object. Useful in case an ambiguous names. To be run before the main script.
Output file are written in a different directory. Change the code to fit the files to change.
Syntax is: ```pyhon3 IRIS-changeObjectName.py```.

