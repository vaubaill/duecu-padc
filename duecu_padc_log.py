import logging
import os

# create logger object
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_fmt = '%(levelname)s %(filename)s %(lineno)d (%(funcName)s) : %(message)s '
sdlr = logging.StreamHandler()
sdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log_file = 'duecu_padc.log'
if os.path.exists(log_file):
    os.remove(log_file)
#fdlr = logging.FileHandler(log_file)
#fdlr.setFormatter(logging.Formatter(fmt=log_fmt))
log.addHandler(sdlr)
# log.addHandler(fdlr)
