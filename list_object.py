"""DU-ECU PADC project.

List objects in images fits.

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""
import os
import glob
from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u

from duecu_padc_log import log

# user's choices
pthi = './'
Lremove = True

# list all fits file in pthi
listfits = glob.glob(pthi+'/*.fit*')
listfits.sort()
# loop over files
for fitsi in listfits:
    # read fits image
    hdr = fits.getheader(fitsi)
    try:
        obj = hdr['OBJECT']
        log.info('OBJECT '+obj+'     '+fitsi)
        if obj=='':
            log.warning('EMPTY OBJECT DETECTED in '+fitsi)
    except:
        msg = 'No OBJECT keyword in '+fitsi
        log.error(msg)
        raise ValueError(msg)
#    ra = hdr['CRVAL1']
#    dec = hdr['CRVAL2']
#    skycoo = SkyCoord(ra*u.deg,dec*u.deg)
#    log.info('RA/DEC: '+skycoo.to_string())
    try:
        ra = hdr['CRVAL1']
        dec = hdr['CRVAL2']
        skycoo = SkyCoord(ra*u.deg,dec*u.deg)
        log.info('RA/DEC: '+skycoo.to_string())
    except:
        msg = 'No RA/DEC info in file '+fitsi
        log.error(msg)
        if Lremove:
            log.warning('File '+fitsi+' will be deleted')
            os.remove(fitsi)
        #raise ValueError(msg)


