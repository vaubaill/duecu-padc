"""DU-ECU PADC project.

List objects in images fits.

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""
import os
import glob
from astropy.io import fits

from duecu_padc_log import log

pthi = './'
# list all fits file in pthi
listfits = glob.glob(pthi+'/*.fit*')
listfits.sort()
# loop over files
for fitsi in listfits:
    # read fits image
    hdr = fits.getheader(fitsi)
    try:
        obj = hdr['FILTER']
        log.info('FILTER '+obj+'     '+fitsi)
        if obj=='':
            log.warning('EMPTY FILTER DETECTED in '+fitsi)
    except:
        msg = 'No FILTER keyword in '+fitsi
        log.error(msg)
        raise ValueError(msg)
    


