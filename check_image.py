"""DU-ECU PADC project.

Check and correct images to be ingested in VO-Paris database.

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""
import os
import re
from datetime import datetime
from astropy.io import fits
import astropy.units as u
from astropy.time import Time

from astroquery.jplsbdb import SBDB
from configparser import ConfigParser, ExtendedInterpolation


from duecu_padc_log import log


def fix_exposure(hdri,bntt=False):
    """Fix exposure time.
    
    Parameters
    ----------
    hdri : astropy.io.fits.Header object
        fits image hedaer to fix.
    bntt : Boolean, optional
        If True, correction for images taken with NBTT camera is performed.
        Default is False.
    
    Returns
    -------
    hdro : astropy.io.fits.Header object
        fixed fits image hedaer.
    
    """
    # copy input header
    hdro = hdri.copy()
    # exposure time comment
    comm = 'Exposure time [s]'
     # change exposure time
    if bntt:
        try:
            hdro.set('EXPTIME', value=hdro['TM-EXPOS'], comment=comm, after='TM-EXPOS')
            log.debug('EXPTIME set from TM-EXPOS value')
        except KeyError:
            hdro.set('EXPTIME', value=10.0, comment=comm)
            log.debug('EXPTIME set at default 10.0 s')
        except:
            msg = '*** Impossible to set exposure time'
            log.error(msg)
            raise ValueError(msg)
    # make sure the comments are ok no matter what
    try:
        hdro.set('EXPTIME', value=hdro['EXPTIME'], comment=comm)
    except KeyError:
        hdro.set('EXPTIME', value=hdro['EXPOSURE'], comment=comm)
    except:
        msg = 'Impossible to set exposure time'
        log.error(msg)
        raise ValueError(msg)
    log.debug('Exposure time updated.')
    return hdro

def fix_date(hdri,bntt=False):
    """Fix date and time.
    
    Parameters
    ----------
    hdri : astropy.io.fits.Header object
        fits image hedaer to fix.
    bntt : Boolean, optional
        If True, correction for images taken with NBTT camera is performed.
        Default is False.
    
    Returns
    -------
    hdro : astropy.io.fits.Header object
        fixed fits image hedaer.
    
    """
    # copy input header
    hdro = hdri.copy()
    # exposure time comment
    comm = 'Date of exposure [isot]'
    if bntt:
        date_iso = datetime.strptime(hdri['DATE'].strip(), '%d/%m/%Y').date().isoformat()
        expdate = Time(date_iso)+hdri['TM-START']*u.s
    else:
        try:
            expdate = Time(hdro['DATE-OBS'])
        except KeyError:
            expdate = Time(hdro['DATE'])
        except:
            msg = 'Impossible to find date in header'
            log.error(msg)
            raise ValueError(msg)
    hdro.set('DATE-OBS',value=expdate.isot,after='EXPTIME',comment=comm)
    return hdro

def fix_astrometry(hdri,bntt=False):
    """Fix astrometry keywords.
    
    Parameters
    ----------
    hdri : astropy.io.fits.Header object
        fits image hedaer to fix.
    bntt : Boolean, optional
        If True, correction for images taken with NBTT camera is performed.
        Default is False.
    
    Returns
    -------
    hdro : astropy.io.fits.Header object
        fixed fits image hedaer.
    
    """
    # copy input header
    hdro = hdri.copy()
    # CRVAL
    log.info('Checking astrometry')
    try:
        log.debug('check CRVAL1')
        hdro['CRVAL1']
        log.debug('CRVAL1 found!!! '+str(hdro['CRVAL1']))
    except KeyError:
        log.error('CRVAL1 NOT found')
#    try:
#        radec = SkyCoord(ra=hdro['OBJCTRA'],dec=hdro['OBJCTDEC'], frame='icrs', unit=(u.hourangle, u.deg))
#        log.info('ra: '+str(radec.ra.to('deg'))+' dec: '+str(radec.dec.to('deg')))
#        hdro.set('CRVAL1',value=radec.ra.to('deg').value,after='OBJECT',comment='Nominal Right Ascension of center of image [deg]')
#        hdro.set('CRVAL2',value=radec.dec.to('deg').value,after='CRVAL1',comment='Nominal Declination of center of image [deg]')    
#    except KeyError:
#        log.warning('OBJCTRA NOT found!')
    # check XPIXSZ
    if bntt:
        try:
            hdro['XPIXSZ']
        except KeyError:
            log.warning('XPIXSZ NOT found adding it. Value=24.4um')
            hdro.set('XPIXSZ',value=24.4,comment='Horizontal Pixel size in [um]')
            hdro.set('YPIXSZ',value=24.4,comment='Vertical Pixel size in [um]')
        try:
            hdro['FOCALLEN']
            log.debug('FOCALLEN is found.')
        except KeyError:
            log.warning('FOCALLEN NOT found adding it. Value=7200.0 mm')
            hdro.set('FOCALLEN',value=7200.0,comment='Focal length in [mm]')
    return hdro

def fix_credit(hdri):
    """Fix date and time.
    
    Parameters
    ----------
    hdri : astropy.io.fits.Header object
        fits image hedaer to fix.
    
    Returns
    -------
    hdro : astropy.io.fits.Header object
        fixed fits image hedaer.
    
    """
    # copy input header
    hdro = hdri.copy()
    hdro.set('OBSERVER',value='DU-ECU',comment='Name of observer.')
    hdro.set('OWNER',value='DU-ECU',comment='Name of image owner.')
    hdro.set('SWOWNER',value='DU-ECU',comment='Name of software owner.')
    
    return hdro


def fix_header(hdri,keywordsconf):
    """Fix header by renaming or replacing cards.
    
    Parameters
    ----------
    hdri : astropy.io.fits.Header object
        fits image hedaer to fix.
    keywordsconf : string
        Keyword configuration file that includes cards to change and to remove. 

    Returns
    -------
    hdro : astropy.io.fits.Header object
        fixed fits image hedaer.
    
    """
    # read keyword configuration file
    if not os.path.exists(keywordsconf):
        msg = 'Keyword configuration file '+keywordsconf+' not found'
        log.error(msg)
        raise IOError(msg)
    confkwd = ConfigParser(interpolation=ExtendedInterpolation())
    confkwd.read(keywordsconf)
    # copy input header
    hdro = hdri.copy()
    
    # fix exposure time
    hdro = fix_exposure(hdro)
    # fix date
    hdro = fix_date(hdro)
    
    # replace keywords
    for k in confkwd['KEY2CHANGE']:
        for k2chg in confkwd['KEY2CHANGE'][k.upper()].split(','):
            try:
                log.debug('rename '+k2chg.upper()+' with '+k.upper())
                hdro.rename_keyword(k2chg.upper(),k.upper())
            except:
                log.debug(k2chg.upper()+' not found in header')
    
    # remove keywords
    for k in confkwd['KEY2REMOVE']:
        hdro.remove(k.upper(), ignore_missing=True)
    
    # remove blank cards
    keepgoing = True
    while keepgoing:
        try:
            hdro.remove('')
        except:
            log.debug('No more empty card to remove')
            keepgoing = False
    
    # add necessary cards
    for k in confkwd['KEY2ADD']:
        value, comm = confkwd['KEY2ADD'][k].split(',')
        hdro.set(k.upper(),value=value.strip(),comment=comm.strip())
    
    # TODO:
    # convert values if needed to International System
    
    return hdro


def verify_header(hdri,keywordsconf):
    """Verify header by ensuring all cards are present.
    
    Parameters
    ----------
    hdri : astropy.io.fits.Header object
        fits image header to check.
    keywordsconf : string
        Keyword configuration file that includes cards that have been fixed in a prior step. 

    Returns
    -------
    None.
    
    """
    # read keyword configuration file
    confkwd = ConfigParser(interpolation=ExtendedInterpolation())
    confkwd.read(keywordsconf)
        
    # ensure keywords are uniform
    for key in confkwd['KEY2CHANGE']:
        # ensure keywords are present
        assert key in hdri, key+' is missing!'
        # loop in modified keys
        for k2chg in confkwd['KEY2CHANGE'][key.upper()].split(','):
            # ensure keywords are absent
            assert k2chg not in hdri, 'extra key ('+key+') found in header!'

    # ensure added keywords are present
    for key in confkwd['KEY2ADD']:
        assert key in hdri, key+' is missing!'

    # ensure removed keywords are absent
    for key in confkwd['KEY2REMOVE']:
        assert key not in hdri, 'extra key ('+key+') found in header!'
        
    log.info('Keywords verification successful.')

    return


def fix_object(hdri,objectconf):
    """Check and (if needed) change the name of the object.
    
    Parameters
    ----------
    hdri : astropy.io.fits.Header object
        fits image header to fix.
    objectconf : string
        Object configuration file that includes object name correspondance. 

    Returns
    -------
    hdro : astropy.io.fits.Header object
        fixed fits image hedaer.
    
    """
    # reads objects configuration files
    confobj = ConfigParser(interpolation=ExtendedInterpolation())
    confobj.optionxform=str
    confobj.read(objectconf)
    # copy input header
    hdro = hdri.copy()
    log.debug('Initial object name: '+hdro['OBJECT'])
    # Get minimal name
#    try:
#        objname = hdro['OBJECT'].split('/')[0]
#    except:
#        objname = hdro['OBJECT']
    objname = hdro['OBJECT']
    # remove all non alphanumeric characters
    objname = re.sub(r'[^a-zA-Z0-9]', ' ', objname)
    # remove leading and trailing blanks
    objname = objname.strip()
    objname = objname.replace(' ','')
    log.info('objname: '+objname)
    
    
    # See if the name is in the objects file
    new_name = ''
    for key in confobj['OBJECTS']:
        log.debug('Trying: '+key.strip()+ ' vs '+objname)
        if (key.strip()==objname):
            new_name = confobj['OBJECTS'][key]
            log.debug('objname found in '+objectconf+' and changed to '+new_name)
    if new_name:
        log.info('Changing '+objname+' into '+new_name)
        objname = new_name
    else:
        msg = 'Object '+objname+' not found: name left as is.'
        log.debug(msg)
    
    # Check name in KPL database
    jpl_name = ''
    sbdb = SBDB.query(objname)
    # check if query successful
    try:
        jpl_name = sbdb['object']['fullname']
    except:
        msg = 'Object: '+objname+' not found in JPL database'
        log.info(msg)   
    if jpl_name:
        log.info('Object found in JPL database. Name changed to: '+jpl_name)
        objname = jpl_name
        # change '/' from comet names to prevent conflicts with astro_reduce scripts.
        objname = objname.replace('/',' ')
        objname = objname.replace('(','')
        objname = objname.replace(')','')
    
    # ceck for NGC type objects
    if objname.startswith('NGC'):
        objname = 'NGC ' + objname.split('NGC')[1]
    # udate object name in header
    hdro.set('OBJECT',value=objname,comment='Target Object name')
    return hdro


def read_fits(in_file):
    """Reads input fits image.
    
    Parameters
    ----------
    in_file : string
        Original fits image file to check.

    Returns
    -------
    hdr : astropy.io.fits.Header object
        fits image hedaer.
    data : astropy.io.fits.data
        Data in in_file.
        
    """    
    
    # read fits image
    log.info('Read '+in_file)
    hdu = fits.open(in_file)
    hdr = hdu[0].header
    data = hdu[0].data
    hdu.close()    
    return hdr,data

def save_fits(hdr,data,ori_file, out_file):
    """Save output fits image.
    
   Parameters
    ----------
    hdr : astropy.io.fits.Header object
        fits image hedaer.
    data : astropy.io.fits.data
        Data to save.
    ori_file : string
        Original fits image file to check.
    out_file : string
        Output fits image file name.

    Returns
    -------
    None.
        
    """
    # add HISTORY and COMMENTS
    hdr.add_history('Original image: '+os.path.basename(ori_file))
    hdr.add_history('Modified by duecu-padc check_image script')
    
    # save output fits file
    hdu = fits.PrimaryHDU(data,header=hdr)
    hdu.verify('fix')
    #hdulist     = fits.HDUList([hdu])
    hdu.writeto(out_file,overwrite=True)
    log.info('Image saved in '+out_file)    
    return


def fix_image(in_file, out_file,keywordsconf='keywords.in',objectconf='objects.in'):
    """

    Parameters
    ----------
    in_file : string
        fits image file to check.
    out_file : string
        Output fits image file name.
    keywordsconf : string, optional
        Keyword configuration file. Default is: 'keywords.in'
    objectconf: string, optional
        Objects names confirugation file. Default is: 'objects.in'

    Returns
    -------
    None.

    """
    
    # read fits image
    hdr_in, data  = read_fits(in_file)   
    # make output header
    hdr_out = hdr_in.copy()
    # performs the corrections:
    hdr_out = fix_header(hdr_in,keywordsconf)
    # change astrometry
    hdr_out = fix_astrometry(hdr_out)
    # change object name if needed
    hdr_out = fix_object(hdr_out,objectconf)
    # save output file
    save_fits(hdr_out,data,in_file,out_file)
    return

def verify_image(in_file,keywordsconf='keywords.in',objectconf='objects.in'):
    """

    Parameters
    ----------
    in_file : string
        fits image file to check.
    keywordsconf : string, optional
        Keyword configuration file. Default is: 'keywords.in'
    objectconf: string, optional
        Objects names confirugation file. Default is: 'objects.in'

    Returns
    -------
    None.

    """
    
    # read fits image
    hdr, data  = read_fits(in_file)   
    # check the header file:
    verify_header(hdr,keywordsconf)
    # check the astrometry
    # verify_astrometry(hdr)
    # check the object name
    # verify_object(hdr,objectconf)

    log.info('File verification complete.')    
    return
