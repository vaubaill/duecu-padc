"""Change headers of images taken with BNTT camera at OHP T120 telescope before 2014

DU-ECU PADC project.
J. Vaubaillon, C. Aykroyd, M. Lemaitre, Observatoire de Paris - PSL, 2024

"""
import os
import glob
import argparse
from datetime import datetime
from astropy.io import fits
import astropy.units as u
from astropy.time import Time
from configparser import ConfigParser, ExtendedInterpolation

from duecu_padc_log import log
from check_image import fix_header, fix_object, read_fits, save_fits, fix_exposure, fix_date, fix_astrometry

def correct_bntt_images(pthi,ptho,imgtype='img',keywordsconf='keywords.in',objectconf='objects.in'):
    """Correct images taken by BNTT camera.
    
    Parameters
    ----------
    pthi : string
        Input directory name.
    ptho : string
        Output directory name.
    imgtype : string, optional
        Image Type. Choice is: 'img', 'dark', 'flat'. Default is 'img'.
    keywordsconf : string, optional
        Keyword configuration file. Default is: 'keywords.in'.
    objectconf: string, optional
        Objects names confirugation file. Default is: 'objects.in'.
    
    Returns
    -------
    None.
    
    """
    # check config file
    if not os.path.isfile(keywordsconf):
        msg = '*** Config file '+keywordsconf+' does not exist'
        log.error(msg)
        raise IOError(msg)
    # reads keywords and objects configuration files
    confkwd = ConfigParser(interpolation=ExtendedInterpolation())
    confkwd.read(keywordsconf)
    # list of images in phti
    listi = glob.glob(pthi+'*.fit*')
    if not listi:
        msg = '*** No fits file in '+pthi
        log.error(msg)
        raise IOError(msg)
    if not os.path.isdir(ptho):
        log.info('Creating directory: '+ptho)
        os.mkdir(ptho)
    for pth in [pthi,ptho]:
        if not pth.endswith('/'):
            pth = pth + '/'
    # loop over files
    for fitsi in listi:
        log.info('*** Processing: '+fitsi)
        # make output file name
        fitso = fitsi.replace(pthi,ptho)
        # read fits image
        hdri, data  = read_fits(fitsi)  
        # make output header
        hdro = hdri.copy()
                
        # fix exposure time
        hdro = fix_exposure(hdro,bntt=True)
        # fix date
        hdro = fix_date(hdro,bntt=True)
        # fix astrometry
        hdro = fix_astrometry(hdro,bntt=True)
        # fix rest of header
        hdro = fix_header(hdro,keywordsconf)
        # fix object name
        hdro = fix_object(hdro,objectconf)
        # save output image
        save_fits(hdro,data,fitsi,fitso)
        
    log.info('Done')
    return



if __name__ == '__main__':
    
    # parse arguments
    parser = argparse.ArgumentParser(description='launch_check_image arguments.')
    parser.add_argument('-i',default='./reduced/',help='Input directory: this is where all input files are located. Default is: ./reduced/')
    parser.add_argument('-o',default='./padc/',help='Output directory: this is where all output files (to be ingested into VO-PADC) will be saved. Default is: /padc/')
    parser.add_argument('-t',default='img',help='Image Type. Choice is: "img", "dark", "flat". Default is "img".')
    args = parser.parse_args()
    
    # set configuration file
    pthi = args.i
    ptho = args.o
    imgtype = args.t
    
    # fix slash syndrome...
    if not pthi.endswith('/'):
        pthi = pthi + '/'
    if not ptho.endswith('/'):
        ptho = ptho + '/'
    
    # launch check_image
    correct_bntt_images(pthi,ptho,imgtype=imgtype)
    
else:
    log.debug('successfully imported')